namespace Domain
{
    public abstract class BaseEntity
    {
        public int id { get; set; }
    }
}