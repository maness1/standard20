import {LogManager, PLATFORM} from "aurelia-framework";
import {Router, RouterConfiguration} from "aurelia-router";

export var log = LogManager.getLogger('MainRouter');

export class MainRouter {
  public router: Router;
  constructor (){
    log.debug('constuctor');
  }

  configureRouter(config: RouterConfiguration, router: Router): void{
    this.router = router;
    config.title = "Contact App-Aurelia";
    config.map(
      [
        {route: ['', 'home'], name: 'home', moduleId: PLATFORM.moduleName('home'), nav: true, title: "home"}
      ]
    );
  }
}
