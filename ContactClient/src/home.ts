import {LogManager, View} from "aurelia-framework";
import {RouteConfig, NavigationInstruction} from "aurelia-router";

export var log = LogManager.getLogger('Home');

export class Home {
  constructor() {
    log.debug('constuctor');
  }

  // VIEW LIFE CYCLE EVENTS
  created(owningView: View, myView: View) {
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached(){
    log.debug('attached');
  }
  detached(){
    log.debug('detached');
  }
  unbind(){
    log.debug('unbind');
  }

  // router LIFE CYCLES
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction){
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction){
    log.debug('activate');
  }

  canDeactivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction){
    log.debug('canDeactivate');
  }

  deactivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction){
    log.debug('deactivate');
  }
}


